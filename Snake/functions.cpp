﻿#define _USE_MATH_DEFINES
#include "functions.h"
#include "audio.h" // include audio (alut)
#include "button.h" // draw buttons
#include "text.h"

#include <algorithm>
#include <alut.h>
#include <cstdlib>
#include <cctype>
#include <cmath>
#include <ctime>
#include <deque>
#include <fstream>
#include <glut.h>
#include <map>
#include <random>
#include <sstream>
#include <string>
#include <utility>

#ifdef _WIN32
    #define sleep(x) // костыль, но этот слип зачем-то нужен был на линуксе
#endif

using point = std::pair<GLfloat, GLfloat>;
using coord = std::deque<point>;
using results = std::multimap<GLint, std::string>;

// <<<<<<<<<<<<------------- evil global variables ---------------->>>>>>>>>>>>
std::mt19937 gen(time (0));
std::uniform_int_distribution<> uid(0, 19);
std::uniform_int_distribution<> uidexit(0, 1);
std::uniform_int_distribution<> uideat(0, 2);

coord snake;
char direction; // направление (l, u, d, r)
char lastDirection; // прошлое направление
GLuint speed = 400; // время между обновлениями
point apple = std::make_pair(uid(gen), uid(gen)); // позиция яблока
bool appleOnScreen = true; // нарисовано ли яблоко
GLuint score = 0; // очки
bool youLost = false; // если проиграли
std::string name = "Username"; // имя пользователя
bool stopAnimation = false; // если нужно остановить анимацию (пауза)
results table; // таблица рекордов
GLint menuStatus = 1; // статус меню (0-нету, 1-New Game, 2-Highscores, 3-Exit)
GLint highscoresMenuStatus = 0; // статус меню хайскоров (0 - нету, 1 - back, 2 - reset)
bool playSound = true;
extern ALuint source[]; // look at audio.cpp

// <<<<<<<<<<<------------- главная функия рисования --------------->>>>>>>>>>>
void display()
{
    glEnable(GL_ALPHA_TEST); // разрешаем альфа составляющую цвета
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // разрешаем мешать цвета
    if (menuStatus)
    {
        menu();
        return;
    }
    if (highscoresMenuStatus)
    {
        highscoresMenu();
        return;
    }
    if (stopAnimation)
        return;
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0., 0., 0.);
    glLineWidth(2);
    glBegin(GL_LINES); // рисуем линию
        glVertex2f(500, 0);
        glVertex2f(500, H);
    glEnd();
    drawSnake();
    drawApple();
    legend();
    if (youLost)
    {
        lost();
        return;
    }
    glutSwapBuffers();
}

// <<<<<<<<<<------------ функция работы с клавиатурой ------------>>>>>>>>>>>>
void keyboard(unsigned char key, GLint /*x*/, GLint /*y*/)
{
    if (menuStatus) // если в главном меню
        switch (key)
        {
            case 'w': // вверх по меню
            case 'W':
                if (menuStatus == 3 || menuStatus == 2)
                    menuStatus--;
                break;

            case 's': // вниз по меню
            case 'S':
                if (menuStatus == 1 || menuStatus == 2)
                    menuStatus++;
                break;

            case 13: // Enter
                switch(menuStatus)
                {
                    case 1: // Enter на New game
                        menuStatus = 0;
                        first();
                        stopAnimation = false;
                        break;
                    case 2: // Enter на Highscores
                        menuStatus = 0;
                        stopAnimation = true;
                        highscoresMenuStatus = 1;
                        break;
                    case 3: // Enter на Exit
                        if (playSound)
                        {
                            alSourcePlay(source[ uidexit(gen) ]);
                            sleep(1);
                        }
                        alutExit();
                        exit(0);
                        break;
                }
                break;

            case 27: // Esc
                if (playSound)
                {
                    alSourcePlay(source[ uidexit(gen) ]);
                    sleep(1);
                }
                alutExit();
                exit(0);
                break;

            case 'm': // музыка
            case 'M':
                playSound = !playSound;
                break;

            default:
                break;
        }

    else if (highscoresMenuStatus) // если в меню рекордов
        switch (key)
        {
            case 'w': // вверх по меню
            case 'W':
                if (highscoresMenuStatus == 1)
                    highscoresMenuStatus++;
                break;

            case 's': // вниз по меню
            case 'S':
                if (highscoresMenuStatus == 2)
                    highscoresMenuStatus--;
                break;

            case 13: // Enter
                if (highscoresMenuStatus == 1) // Enter на Back
                {
                    highscoresMenuStatus = 0;
                    menuStatus = 1;
                }
                else // Enter на Reset
                if (highscoresMenuStatus == 2)
                {
                    highscoresMenuStatus = 0;
                    menuStatus = 1;
                    // заменяем файл рекордов на пустой файл
                    std::ofstream o("highscores.dat", std::ios::trunc);
                    o.close();
                    // очищаем контейнер с рекордами
                    table.clear();
                }
                break;

            case 27: // Esc
                highscoresMenuStatus = 0;
                menuStatus = 1;
                break;

            case 'm': // музыка
            case 'M':
                playSound = !playSound;
                break;

            default:
                break;
        }

    else if (youLost) // если в окне проигрыша
        switch(key)
        {
            case 8: // если Backspace
                if (!name.empty())
                    name.erase(name.end() - 1);
                break;

            case 127: // если Delete
                if (!name.empty())
                    name.erase(name.begin());
                break;

            case 13: // если Enter
                if (!highscoresMenuStatus)
                {
                    writeResult();
                    menuStatus = 1;
                }
                break;

            case 27: // если Esc
                if (playSound)
                {
                    alSourcePlay(source[ uidexit(gen) ]);
                    sleep(1);
                }
                alutExit();
                exit(0);
                break;

            default: // вводим имя
                if (name.length() < 9) // если длина имени меньше 9
                {
                    if (key == ' ')
                        name += '_';
                    else
                        name += key;
                }
                break;
        }

    else // если в режиме игры
    switch(key)
    {
        case 'a':
        case 'A':
            if (lastDirection != 'r')
                direction = 'l';
            break;

        case 'w':
        case 'W':
            if (lastDirection != 'd')
                direction = 'u';
            break;

        case 's':
        case 'S':
            if (lastDirection != 'u')
                direction = 'd';
            break;

        case 'd':
        case 'D':
            if (lastDirection != 'l')
                direction = 'r';
            break;

        case 'p':
        case 'P':
            if (!youLost)
                pauseGame();
            break;

        case 27:
            if (stopAnimation)
            {
                if (playSound)
                {
                    alSourcePlay(source[ uidexit(gen) ]);
                    sleep(1);
                }
                alutExit();
                exit(0);
            }
            break;

        case 'm': // музыка
        case 'M':
            playSound = !playSound;
            break;

        default:
            break;
    }
    glutPostRedisplay();
}

void special(GLint key, GLint /*x*/, GLint /*y*/)
{
    switch(key)
    {
        case GLUT_KEY_LEFT:
            if (lastDirection != 'r')
                direction = 'l';
            break;

        case GLUT_KEY_UP:
            if (menuStatus == 3 || menuStatus == 2)
                menuStatus--;
            else if (highscoresMenuStatus == 1)
                highscoresMenuStatus++;
            else if (lastDirection != 'd')
                direction = 'u';
            break;

        case GLUT_KEY_DOWN:
            if (menuStatus == 1 || menuStatus == 2)
                menuStatus++;
            else if (highscoresMenuStatus == 2)
                highscoresMenuStatus--;
            else if (lastDirection != 'u')
                direction = 'd';
            break;

        case GLUT_KEY_RIGHT:
            if (lastDirection != 'l')
                direction = 'r';
            break;

        default:
            break;
    }
    glutPostRedisplay();
}

// <<<<<<<<<<<-------------------- меню игры -------------------->>>>>>>>>>>>>>
void menu()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glPointSize(1);
    glBegin(GL_POLYGON);
        glColor3f(0., 1., 0.5);
        glVertex2f(W, H);
        glVertex2f(0, H);
        glColor3f(0.7, 1., 0.);
        glVertex2f(0, 0);
        glVertex2f(W, 0);
    glEnd();

    drawButton(7*W/24, 35*H/48 - menuStatus * 8*H/48 - 5, 10*W/24, 5*H/48 + 5);

    glColor3f(0.9, 0., 0.);
    glLineWidth(3);
    printText(W/4, 3*H/4, 1, "Snake");
    glLineWidth(2);
    printText(W/3, 7*H/12, 0.3, "New game");
    printText(W/3, 5*H/12, 0.3, "Highscores");
    printText(5*W/12, 3*H/12, 0.3, "Exit");
    glutSwapBuffers();
}

// <<<<<<<<<<<------------ функция вывода текста в окно ------------->>>>>>>>>>
void printText(GLfloat x, GLfloat y, GLfloat size, std::string str)
{
    std::string::size_type c = 0;
    glPushMatrix();
    glTranslatef(x, y, 0);
    glScalef(size, size, 0);
    while(c != str.length())
        glutStrokeCharacter(GLUT_STROKE_ROMAN, str[c++]);
    glPopMatrix();
}

// <<<<<<<<<<<<<<--------------- повторяемая функция --------------->>>>>>>>>>>
void replay(GLint /*t*/)
{
    if (stopAnimation)
    {
        glutTimerFunc(speed, replay, 0); // 0 - номер таймера
        return;
    }
    switch(direction)
    {
        case 'l':
            snake.emplace_back((*(snake.end()-1)).first - 1, (*(snake.end()-1)).second);
            break;
        case 'u':
            snake.emplace_back((*(snake.end()-1)).first, (*(snake.end()-1)).second + 1);
            break;
        case 'd':
            snake.emplace_back((*(snake.end()-1)).first, (*(snake.end()-1)).second - 1);
            break;
        case 'r':
            snake.emplace_back((*(snake.end()-1)).first + 1, (*(snake.end()-1)).second);
            break;
        default:
            break;
    }
    // если границы экрана
    if ( (*(snake.end()-1)).first == -1 || (*(snake.end() - 1)).second == -1 ||
        // или попали в себя
         (*(snake.end()-1)).first == 20 || (*(snake.end() - 1)).second == 20 ||
         std::find(snake.begin(), snake.end(), *(snake.end() - 1) ) != snake.end() - 1)
    {
        snake.pop_back();
        lost();
        if (playSound)
        {
            alSourcePlay(source[2]); // проиграываем oy.wav
            if (score >= 50) // хороший результат
            {
                sleep(1);
                alSourcePlay(source[6]);
            }
            else if (score < 10) // плохой результат
            {
                sleep(1);
                alSourcePlay(source[7]);
            }
        }
    }
    else
    {
        glutTimerFunc(speed, replay, 0); // 0 - номер таймера
        glutPostRedisplay();
    }
    if (*(snake.end() - 1) == apple) // если скушали яблоко
    {
        if (playSound)
            alSourcePlay(source[3 + uideat(gen)]); // проиграть рандомное чавканье
        score++;
        speed *= 0.97;
        appleOnScreen = false;
    }
    else if (!youLost) // если не проиграли
        snake.pop_front(); // удаляем конец змейки
    lastDirection = direction;
}

// <<<<<<<<<<<<<<-------------- рисует змейку -------------------->>>>>>>>>>>>>
void drawSnake()
{
    glColor3f(0., 0., 0.);
    for (auto it = snake.begin(); it != snake.end(); ++it)
    {
        glBegin(GL_POLYGON);
            glVertex2f( (*it).first * N + 1, (*it).second * N + 1);
            glVertex2f( ((*it).first + 1) * N - 1, (*it).second * N + 1);
            glVertex2f( ((*it).first + 1) * N - 1, ((*it).second + 1) * N - 1);
            glVertex2f( (*it).first * N + 1, ((*it).second + 1) * N - 1);
        glEnd();
    }

    // рисуем глазки
    float R1x = (*snake.rbegin() ).first * N, R2x = R1x,
          R1y = (*snake.rbegin() ).second * N, R2y = R1y;
    switch(lastDirection)
    {
        case 'l':
            R1x += N / 4;
            R1y += 3*N / 4;
            R2x += N / 4;
            R2y += N / 4;
            break;
        case 'u':
            R1x += N / 4;
            R1y += 3*N / 4;
            R2x += 3*N / 4;
            R2y += 3*N / 4;
            break;
        case 'd':
            R1x += N / 4;
            R1y += N / 4;
            R2x += 3*N / 4;
            R2y += N / 4;
            break;
        case 'r':
            R1x += 3*N / 4;
            R1y += 3*N / 4;
            R2x += 3*N / 4;
            R2y += N / 4;
            break;
        default:
            break;
    }

    glColor3f(0., 0.7, 0.);
    glBegin(GL_POLYGON);
        for (GLfloat omega = 0; omega < 2*M_PI; omega += M_PI / 25)
            glVertex2f(R1x + cos(omega) * (N/4+1), R1y + sin(omega) * (N/4+1) );
    glEnd();
    glBegin(GL_POLYGON);
        for (GLfloat omega = 0; omega < 2*M_PI; omega += M_PI / 25)
            glVertex2f(R2x + cos(omega) * (N/4+1), R2y + sin(omega) * (N/4+1) );
    glEnd();

    glColor3f(0., 0., 0.);
    glPointSize(3);
    glBegin(GL_POINTS);
        glVertex2f(R1x, R1y);
        glVertex2f(R2x, R2y);
    glEnd();
}

// <<<<<<<<<<------------- задает начальное значение ------------>>>>>>>>>>>>>>
void first()
{
    snake.clear(); // очищаем значения со змейки
    snake.emplace_back(12, 12); // задаем начальные координаты
    snake.emplace_back(13, 12);
    direction = 'r'; // задаем направление
    lastDirection = 'l';
    youLost = false;
    drawApple(); // рисуем яблоко
    replay(1); // функция повтора
    score = 0; // обнуляем очки
    speed = 400; // сбрасываем скорость
    highscoresMenuStatus = 0;
}

// <<<<<<<<<<<<<<<<<<---------------- рисует яблоко -------------->>>>>>>>>>>>>
void drawApple()
{
    if (!appleOnScreen)
        do
        {
            apple.first = uid(gen);
            apple.second = uid(gen);
            appleOnScreen = true;
        } while (std::find(snake.begin(), snake.end(), apple) != snake.end() );
    glColor3f(1., 0., 0.);
    glBegin(GL_POLYGON);
        for (GLfloat omega = 0; omega < 2*M_PI; omega += M_PI / 25)
            glVertex2f(apple.first * N + N/2 + cos(omega) * 12,
                       apple.second * N + N/2 + sin(omega) * 12);
    glEnd();
}

// <<<<<<<<------------------ функция при проигрыше ------------------>>>>>>>>>
void lost()
{
    glColor4f(1., 1., 1., 0.8); // накладываем пелену, обесцвечиваем задний фон
    glBegin(GL_POLYGON);
        glVertex2f(0, 0);
        glVertex2f(0, H);
        glVertex2f(W, H);
        glVertex2f(W, 0);
    glEnd();
    glColor3f(0., 0., 0.);
    glLineWidth(3);
    printText(W/4, 5*H/6, 0.5, "You lose");
    glColor3f(0., 0., 0.);
    printText(W/9, 4*H/6, 0.35, "Enter your name");
    glColor3f(1., 0., 0.);
    printText(W/4, 3*H/6, 0.35, name);
    glColor3f(0., 0., 1.);
    printText(W/5, H/6, 0.25, "Press");
    glColor3f(1., 0., 0.);
    printText(5*W/14, H/6, 0.25, "Esc");
    glColor3f(0., 0., 1.);
    printText(13*W/28, H/6, 0.25, "to exit");
    glutSwapBuffers();
    youLost = true;
}

// <<<<<<<<<<<<<<<<--------------- рисует подписи ----------------->>>>>>>>>>>>
void legend()
{
    glLineWidth(3);
    glColor3f(0., 1., 0.1);
    glWrite(505, 11*H/12, 7, "Your score:");
    glColor3f(1., 0., 0.);
    std::ostringstream oss;
    oss << score;
    printText(score < 10 ? 565 : 530, 37*H/48, 0.5, oss.str());
    glColor3f(0., 0., 1.);
    // рисуем таблицу рекордов
    highscores();
    GLfloat height = 2*H/3 - 10;
    glWrite(505, height + H/24 + 10, 7, "Highscores:");
    glColor3f(0., 0., 0.);
    for (auto it = table.rbegin(); it != table.rend(); ++it)
    {
        glColor3f(0., 0., 0.);
        glWrite(505, height, 6, (*it).second);
        glColor3f(1., 0., 0.);
        glWrite(605, height, 6, (*it).first);
        height -= H/24;
    }

    // <<<<<<<<<<<<<<------------ рисуем громофон ---------------->>>>>>>>>>>>>>
    glColor3f(0., 0., 1.);
    glBegin(GL_LINE_STRIP);
        glVertex2i(535, 50);
        glVertex2i(560, 50);
        glVertex2i(560, 80);
        glVertex2i(535, 80);
        glVertex2i(535, 50);
        glVertex2i(560, 50);
        glVertex2i(580, 35);
        glVertex2i(580, 95);
        glVertex2i(560, 80);
    glEnd();

    if (!playSound)
    {
        glColor3f(1., 0., 0.);
        glBegin(GL_LINE_LOOP);
            for (GLfloat omega = 0; omega < 2*M_PI; omega += M_PI / 25)
                glVertex2f(560 + cos(omega) * 40, 65 + sin(omega) * 40);
        glEnd();
        glBegin(GL_LINES);
            glVertex2f(560 + cos(M_PI / 4) * 40, 65 + sin(M_PI / 4) * 40);
            glVertex2f(560 - cos(M_PI / 4) * 40, 65 - sin(M_PI / 4) * 40);
        glEnd();
    }

    glColor3f(0., 1., 0.);
    glWrite(525, H/48, 5, "Music: ");
    glColor3f(1., 0., 0.);
    glWrite(585, H/48, 5, (playSound ? "on" : "off") );
}

// <<<<<<<<<<<<<------------------- пауза------------------------>>>>>>>>>>>>>
void pauseGame()
{
    // если в окне меню
    if (menuStatus || highscoresMenuStatus)
        return;
    stopAnimation = !stopAnimation;
    if (stopAnimation)
    {
        glColor4f(1., 1., 1., 0.5);
        glBegin(GL_POLYGON);
            glVertex2f(0, 0);
            glVertex2f(0, H);
            glVertex2f(W, H);
            glVertex2f(W, 0);
        glEnd();
        glColor3f(0., 0., 0.);
        printText(W/4, 3*H/4, 0.5, "Pause");
        glColor3f(0., 0., 1.);
        printText(5*W/32, 5*H/9, 0.25, "Press");
        glColor3f(1., 0., 0.);
        printText(10*W/32, 5*H/9, 0.25, "p");
        glColor3f(0., 0., 1.);
        printText(12*W/32, 5*H/9, 0.25, "to continue");
        glColor3f(0., 0., 1.);
        printText(W/5, 3*H/9, 0.25, "Press");
        glColor3f(1., 0., 0.);
        printText(5*W/14, 3*H/9, 0.25, "Esc");
        glColor3f(0., 0., 1.);
        printText(13*W/28, 3*H/9, 0.25, "to exit");
        glutSwapBuffers();
    }
}

// <<<<<<<<<<<<<<---------- вспомогательная функция -------------->>>>>>>>>>>>>
bool isInt(const std::string &s)
{
    for (auto it = s.begin(); it != s.end(); ++it)
        if (!isdigit(*it))
            return false;
    return true;
}

// <<<<<<<<<<<<----------- считывает таблицу рекордов ------------->>>>>>>>>>>>
void highscores()
{
    std::ifstream ifs("highscores.dat");
    if (!ifs)
        return;
    std::string str, str2;
    GLint d;
    if (table.empty())
        for (std::size_t i = 0; i < 10; ++i)
        {
            // если удалось считать с потока
            if (ifs >> str >> str2)
            {   // если вторая строка является целым числом в диапазоне [0, 99)
                if (isInt(str2) && str2.length() < 3)
                {
                    if (str.size() > 9)
                        str.resize(9);
                    std::stringstream(str2) >> d;
                    // добавляем элемент в мап
                    table.emplace(d, str);
                }
            }
            else
                break;
        }
    ifs.close();
}

// <<<<<<<<<<<<<<-------------- меню highscore ----------------->>>>>>>>>>>>
void highscoresMenu()
{
    // задний фон
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_POLYGON);
        glColor3f(0., 1., 0.5);
        glVertex2f(W, H);
        glVertex2f(0, H);
        glColor3f(0.7, 1., 0.);
        glVertex2f(0, 0);
        glVertex2f(W, 0);
    glEnd();

    // выделение текущего выбора
    drawButton(8*W/24, -3*H/48 + highscoresMenuStatus * 6*H/48, 200, 50);

    // рисуем рекорды
    glColor3f(0.9, 0., 0.);
    glLineWidth(3);
    printText(W/4, 5*H/6, 0.5, "Highscores");
    glLineWidth(2);
    printText(5*W/12 - 5, 5*H/24, 0.3, "Reset");
    printText(5*W/12, H/12, 0.3, "Exit");
    GLint height = 3*H/4;
    highscores();
    for (auto it = table.rbegin(); it != table.rend(); ++it)
    {
        glColor3f(0., 0., 0.);
        glWrite(W/4, height, 7, (*it).second);
        glColor3f(1., 0., 0.);
        glWrite(3*W/4, height, 7, (*it).first);
        height -= H/20;
    }
    glutSwapBuffers();
}

// <<<<<<<<<<<<---------- записывает таблицу рекордов ----------->>>>>>>>>>>>>>
void writeResult()
{
    std::ofstream o("highscores.dat");
    table.emplace(score, name);
    GLuint counter = 0;
    for (auto it = table.rbegin(); it != table.rend() && counter < 10; ++it)
    {
        if (((*it).second).size() > 9)
            ((*it).second).resize(9);
        o << (*it).second << " " << (*it).first << std::endl;
        counter++;
    }
    table.clear();
    o.close();
}
