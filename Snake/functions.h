﻿#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include <glut.h>
#include <string>

#define W 630 // 500 - поле, 130 - лабуда
#define H 500
#define N 25 // сторона квадрата, поле 25*25 квадратов

// <<<<<<<<<<<<<--------------прототипы функций------------->>>>>>>>>>>
void display();
void keyboard(unsigned char key, GLint x, GLint y);
void special(GLint key, GLint x, GLint y);
void menu(); // меню игры
void printText(GLfloat x, GLfloat y, GLfloat size, std::string text);
void replay(GLint);
void drawSnake();
void first();
void drawApple();
void lost(); // когда проиграли
void legend(); // всякие подписи
void pauseGame(); // пауза
void highscores(); // рекорды
void highscoresMenu(); // меню highscore
void writeResult(); // записываем результат

#endif
