﻿#include "audio.h"

// define for use deprecated functions in alut
#define MIDL_PASS
#include <alut.h>
#include <iostream>

#define NUM_BUFFERS 8
#define NUM_SOURCES 8
#define NUM_ENVIRONMENTS 1

/* ------------------ *
0, 1 - exit
2 - oy
3, 4, 5 - eat
6 - good
7 - bad
* ------------------- */

// <<<<<<<<<<<<------------ sounds global variables -------------->>>>>>>>>>>>>
ALfloat listenerPos[] = {0.0, 0.0, 4.0};
ALfloat listenerVel[] = {0.0, 0.0, 0.0};
ALfloat	listenerOri[] = {0.0, 0.0, 1.0, 0.0, 1.0, 0.0};
ALfloat sourcePos[] = {2.0, 0.0, 0.0};
ALfloat sourceVel[] = {0.0, 0.0, 0.0};

ALuint buffer[NUM_BUFFERS];
ALuint source[NUM_SOURCES];
ALuint environment[NUM_ENVIRONMENTS];

ALsizei size, freq;
ALenum format;
ALvoid *data;
int ch;
ALboolean loop;

// <<<<<<<<<<<<<<<<<<----------- audio init ------------->>>>>>>>>>>>>>>>>>>>>>>
bool audioInit(int *argc, char **argv)
{
    alutInit(argc, argv);

    alListenerfv(AL_POSITION, listenerPos);
    alListenerfv(AL_VELOCITY, listenerVel);
    alListenerfv(AL_ORIENTATION, listenerOri);
    
    alGetError(); // clear any error messages
    
    if (alGetError() != AL_NO_ERROR)
    {
        std::cerr << "Error creating buffers\n";
        return false;
    }
    
    // Generate buffers, or else no sound will happen!
    alGenBuffers(NUM_BUFFERS, buffer);
    
    alutLoadWAVFile( (ALbyte *)"sound/exit1.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[0], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/exit2.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[1], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/oy.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[2], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/eat1.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[3], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/eat2.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[4], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/eat3.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[5], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/good.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[6], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);
    
    alutLoadWAVFile( (ALbyte *)"sound/bad.wav", &format, &data, &size, &freq, &loop);
    alBufferData(buffer[7], format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);

    alGetError(); /* clear error */
    alGenSources(NUM_SOURCES, source);

    if (alGetError() != AL_NO_ERROR) 
    {
        std::cout << "Error creating sources\n";
        return false;
    }
    
    for (int i = 0; i < NUM_SOURCES; i++)
    {
        alSourcef(source[i], AL_PITCH, 1.0f);
        alSourcef(source[i], AL_GAIN, 1.0f);
        alSourcefv(source[i], AL_POSITION, sourcePos);
        alSourcefv(source[i], AL_VELOCITY, sourceVel);
        alSourcei(source[i], AL_BUFFER, buffer[i]);
        alSourcei(source[i], AL_LOOPING, AL_FALSE);
    }
    
    return true;
}
