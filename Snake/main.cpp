﻿/* *************************************************************************
*** Игра змейка. Управление w a s d. Пауза - p.                          ***
*** Внимание, имя пользователя не должно содержать кириллицы! Таблица    ***
*** рекордов содержится в файле highscore.txt                            ***
*** Date: 18.06.2012                                                     ***
*** Made by: Litvinov D. M.                                              ***
************************************************************************* */
#include "audio.h"
#include "functions.h"

// <<<<<<<<<<<<<-------------------- main ----------------->>>>>>>>>>>>>>>
int main(int argc, char *argv[])
{
    // initialise OpenAL
    if (!audioInit(&argc, argv))
        return 1;
        
    glutInit(&argc, argv);
    glutInitWindowSize(W, H);
    glutInitWindowPosition(100,100);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutCreateWindow("Snake");
    glClearColor(1., 1., 1., 1.);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, W, 0, H, 0, 1);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutDisplayFunc(display);
    menu();
    glutMainLoop();
}
