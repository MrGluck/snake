﻿#define _USE_MATH_DEFINES
#include "button.h"

#include <cmath>

void drawButton(GLint x, GLint y, GLint w, GLint h)
{
    glColor3f(1., 1., 0.);
    const float r = w / 10.0;
    glBegin(GL_POLYGON);
        for (GLfloat omega = 0; omega < M_PI / 2; omega += M_PI / 25)
            glVertex2f(x + w - r + cos(omega) * r, y + h - r + sin(omega) * r);
        glColor3f(1., 1., 1.);
        for (GLfloat omega = M_PI / 2; omega < M_PI; omega += M_PI / 25)
            glVertex2f(x + r + cos(omega) * r, y + h - r + sin(omega) * r);
        glColor3f(1., 1., 0.);	
        for (GLfloat omega = M_PI; omega < 3*M_PI / 2; omega += M_PI / 25)
            glVertex2f(x + r + cos(omega) * r, y + r + sin(omega) * r);
        glColor3f(1., 1., 1.);
        for (GLfloat omega = 3*M_PI / 2; omega < 2*M_PI; omega += M_PI / 25)
            glVertex2f(x + w - r + cos(omega) * r, y + r + sin(omega) * r);
    glEnd();
}
