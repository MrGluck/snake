﻿#ifndef _TEXT_H_
#define _TEXT_H_

#include <glut.h>
#include <sstream>
#include <string>

template <typename T>
void glWrite(GLfloat x, GLfloat y, GLint size, T text)
{
    glRasterPos2f(x, y);
    void *font = GLUT_BITMAP_TIMES_ROMAN_24;
    switch (size)
    {
      case 1:
          font = GLUT_BITMAP_TIMES_ROMAN_10;
          break;
      case 2:
          font = GLUT_BITMAP_HELVETICA_10;
          break;
      case 3:
          font = GLUT_BITMAP_HELVETICA_12;
          break;
      case 4:
          font = GLUT_BITMAP_8_BY_13;
          break;
      case 5:
          font = GLUT_BITMAP_9_BY_15;
          break;
      case 6:
          font = GLUT_BITMAP_HELVETICA_18;
          break;
      default:
          font = GLUT_BITMAP_TIMES_ROMAN_24;
          break;
    }
    std::ostringstream oss;
    oss << text;
    std::string str = oss.str();
    for (size_t i = 0; i < str.size(); i++)
        glutBitmapCharacter(font, str[i]);
}

#endif // _TEXT_H_
